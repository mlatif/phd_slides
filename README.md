# CSI_2022

Supports de présentation:
- CSI 2022
- Réunion méthodologie CHU  02/06/22
- Ecole d'Etez 2022
- ...

#### Utilisation du script TIFTEX pour la compile from scratch
[Lien vers le repo](https://gitlab.com/mlatif/tif-tex)

#### Mantra
```
cd existing_repo
git remote add origin https://gitlab.univ-nantes.fr/latif-m/csi_2022.git
git branch -M main
git push -uf origin main
```
